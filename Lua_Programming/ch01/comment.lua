print("This is normal code.")
-- This is a comment
print("This is still normal code.") -- This is a comment at the end of a line of code

--[==[
	This is a comment that contains a closing long bracket of level 0 which is here:
]]
	However, the closing double bracket doesn't make the comment end, because the
	comment was opened with an opening long bracket of level 2, and only a closing
	long bracket of level 2 can close it.
]==]