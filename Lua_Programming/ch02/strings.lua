print("\n\"\'\\\t\17\
	carlos\
	alejandro\
	rosas\
	montano")

-- Is a string type
print(type([[
This is a line that can continue
on more than one line.\n
It can contain single quotes, double quotes and everything else (-- including
comments). It ignores everything (including escape characters) except closing
long brackets of the same level as the opening long bracket.
]]))

-- String bracket.
print([[
This is a line that can continue
on more than one line.

It can contain single quotes, double quotes and everything else (-- including
comments). It ignores everything (including escape characters) except closing
long brackets of the same level as the opening long bracket.
]])

-- String double quote. As previous example, but using \ to escape strings
print("This is a line that can continue\
on more than one line.\
\
It can contain single quotes, double quotes and everything else (-- including\
comments). It ignores everything (including escape characters) except closing\
long brackets of the same level as the opening long bracket.\
")

-- # or #() operator to get the length of a string
print(#[[
	longitud
	mas]])

print(#("\
	longitud\
	mas"))

-- Common concatenation
print("snow".."ball")
print("snow".."ball"..5.6)

-- Non possible concatenations
--print(""..nil)		-- error: attempt to concatenate a nil value
--print(4..5)			-- error: malformed number near
--print("true"..true)	-- error: attempt to concatenate a boolean value
--print("snow"..4.." ")	-- error: malformed number near '4..'

print("snow".."ball"..tostring(true)..tostring(nil)..tostring(4)..tostring(5.6))

-- Concat string double quote with string bracket
print("snow"..[[ball]])

-- Concat long string bracket with long string double quote escaped
print([[This is a line that can continue
on more than one line.

]]
..
"It can contain single quotes, double quotes and everything else (-- including\
comments). It ignores everything (including escape characters) except closing\
long brackets of the same level as the opening long bracket.\
")

-- Coercion
print("122" + 1) --> 123
print("The number is " .. 5 .. ".") --> The number is 5.

-- To be able to concat two numbers, nil or boolean values, is neccesary to use tostring() or tonumber() functions	
print("snow".."ball"..tostring(true)..tostring(nil)..tostring(4)..tostring(5.6)..tonumber("10"))