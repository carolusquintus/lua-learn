-- All logical operators consider "false" and "nil" as "false" and anything else as "true".
-- The operator "and" returns its first argument if it is "false"; otherwise, it returns its second argument.
-- The operator "or" returns its first argument if it is "not false"; otherwise, it returns its second argument:

-- Both "and" AND "or" use short-cut evaluation, that is, they evaluate their second operand only when necessary.
-- Or what is the same: "and" AND "or" return their arguments rather than a boolean value.
print(4 and 5)		--> 5 : (4 == true then returns second value)
print(4 or 5)		--> 4 : (4 == true then returns firs value)
print(nil and 13)	--> nil
print(nil and false)--> nil
print(false and nil)--> false
print(false and 13)	--> false
print(true and 13)	--> 13
print(false or 5)	--> 5

-- equivalent to elvis operator: (a ? b : c)
print(4 > 5 and 6 or 7)	--> 7
print(8 > 5 and 6 or 7) --> 6

--Bitwise Operators
print(3 & 5)	-- bitwise and
print(3 | 5)	-- bitwise or
print(3 ~ 5)	-- bitwise xor
print(7 >> 1)	-- bitwise left shift
print(7 << 1)	-- bitwise left shift
print(~7)		-- bitwise not

-- Operator precedence
-- 1.	Exponentiation: ^
-- 2.	Unary operators: not, #, -, ~
-- 3.	Level 2 mathematical operators: *, /, //, %
-- 4.	Level 1 mathematical operators: +, -
-- 5.	Concatenation: ..
-- 6.	Bit shifts: <<, >>
-- 7.	Bitwise AND: &
-- 8.	Bitwise XOR: ~
-- 9.	Bitwise OR: |
-- 10.	Relational operators: <, >, <=, >=,  ̃=, ==
-- 11.	Boolean and: and
-- 12.	Boolean or: or