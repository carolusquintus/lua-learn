local number = 6

if number < 10 then
	print( "The number " .. number .. " is smaller than ten" )
end

local number = 1000

if number < 10 then
	print( "The number is smaller than ten." )
elseif number < 100 then
	print( "The number is bigger than or equal to ten, but smaller than one hundred." )
elseif number ~= 1000 and number < 3000 then
	print( "The number is bigger than or equal to one hundred, smaller than three thousand and is not exactly one thousand." )
else
	print( "The number is either 1000 or bigger than 2999." )
end