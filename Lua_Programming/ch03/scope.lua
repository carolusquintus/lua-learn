-- Local variable that can be accesed from anywhere in the script
local variable = 13

-- This statement creates a new block and also a new scope
do
	variable = variable + 5		-- This adds 5 to the variable, which now equals 18.
	local variable = 17			-- Local variable to this scope, with the same name as the previous 
	variable = variable - 1		-- Substracts 1 from the local variable of this scope, which now equals 16

	print(variable)	--> 16
end

print(variable)		--> 18