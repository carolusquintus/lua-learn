-- Augmented Assignment
-- In Lua no exists syntactic sugar like C lanaguages a += 8
a = 7
a = a + 9

print(a)

-- Parallel Assignment
a, b, c = 0, 0, 0, 0

print(a, b, c, d)

-- Permutation
first, second = 54, 87
print(first, second)
first, second = second, first
print(first, second)

dictionary = {}
index = 2
index, dictionary[index] = index - 1, 12

print( dictionary, index, dictionary[index], dictionary[2] )