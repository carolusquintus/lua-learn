local number1 = 0

print( "number1" )
while number1 < 10 do
	print( number1 )
	number1 = number1 + 1 -- Increase the value of the number1 by one
end

local number2 = 0

print( "number2" )
repeat
	print( number2 )
	number2 = number2 + 1
until number2 >= 10

print( "number3" )
for number3 = 0, 9, 1 do
	print( number3 )
end

print( "number4" )
for number4 = 0, 2, 0.1 do
	print( number4 )
	if number4 >= 1.5 then
		break -- Terminate the loop instantly and do not repeat
	end
end